/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.primefacesmaventest.mbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author valdemar.arantes
 */
@ManagedBean
@RequestScoped
public class Hello {
    public String getMessage() {
        return "Olá, Primefaces + Maven!";
    }
}
